<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
		<xsl:template match="/AsMinhasMusicas">	
		<xsl:apply-templates select="Albums"/>
		</xsl:template>
				<xsl:template match="Albums">	
				<xsl:element name="Albums">
		<xsl:apply-templates select="Album"></xsl:apply-templates>
		</xsl:element>
		</xsl:template>
			
		<xsl:template match="Album">
			<xsl:element name="Album">
				<xsl:copy-of select="@Nome_A"/>
			</xsl:element>
	</xsl:template>
	
</xsl:stylesheet>
