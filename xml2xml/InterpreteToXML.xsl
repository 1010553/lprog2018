<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/AsMinhasMusicas">
		<xsl:apply-templates select="Interpretes"/>
	</xsl:template>
	<xsl:template match="Interpretes">
		<xsl:element name="Interpretes">
			<xsl:apply-templates select="Interprete"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Interprete">
		<xsl:element name="Interprete">
			<xsl:apply-templates select="Nome_i"/>
			<xsl:apply-templates select="id_i"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Nome_i">
		<xsl:element name="Nome_i">
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="id_i">
		<xsl:attribute name="id_i"><xsl:apply-templates></xsl:apply-templates></xsl:attribute>
	</xsl:template>
</xsl:stylesheet>
