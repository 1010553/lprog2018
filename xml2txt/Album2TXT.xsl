<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
	<xsl:output method="text" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/AsMinhasMusicas">
		<xsl:for-each-group select="Albums/Album" group-by="Imagem">
			<xsl:value-of select="@Nome_A"/>
			<xsl:text>&#xA;</xsl:text>
			<xsl:for-each select="current-group()">
				<!--			
				<xsl:text> - </xsl:text>
				<xsl:value-of select="Musica/@Nome"/>
-->
				<xsl:call-template name="Musicas"/>
				<xsl:text>&#xA;</xsl:text>
			</xsl:for-each>
		</xsl:for-each-group>
	</xsl:template>
	<xsl:template name="Musicas">
		<xsl:for-each select=".">
			<xsl:apply-templates select="Musica"/>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="Musica">
		<xsl:text> - </xsl:text>
		<xsl:value-of select="@Nome"/>
		<xsl:text>&#xA;</xsl:text>
	</xsl:template>
</xsl:stylesheet>
