<?xml version="1.0" encoding="UTF-8"?>
<!-- edited with XMLSpy v2018 rel. 2 (x64) (http://www.altova.com) by João Resende (ISEP) -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:import-schema schema-location="../Music.xsd"/>
	<xsl:output method="html" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/AsMinhasMusicas">
		<html>
			<head>
				<title/>
			</head>
			<body>
				<xsl:for-each-group select="Albums/Album/Musica" group-by="Genero">
					<table border="1" width="100%">
						<tr>
							<td align="center">
								<h1>
									<xsl:value-of select="Genero"/>
								</h1>
							</td>
						</tr>
					</table>
					<table border="1" width="100%">
						<tr>
							<th>Album</th>
							<th>Musica</th>
							<th>Interprete</th>
							<th>Duração</th>
							<th>Video</th>
						</tr>
						<xsl:for-each select="current-group()">
							<tr>
								<td width="1%">
									<xsl:value-of select="position()"/>
									<img src="{../Imagem}" width="100px" height="100px" align="middle"/>
								</td>
								<td width="30%" align="center">
									<xsl:value-of select="@Nome"/>
								</td>
								<td>
									<xsl:call-template name="Interpretes"/>
									<!--	<xsl:value-of select="/AsMinhasMusicas/Interpretes/Interprete[@id_i=current()/Interprete/@id_i]/Nome_i"/> -->
								</td>
								<td width="3%">
									<xsl:value-of select="Duracao"/>
								</td>
								<td width="26%">
									<a href="{Videoclip}" target="_blank">
										<xsl:value-of select="Videoclip"/>
									</a>
								</td>
							</tr>
						</xsl:for-each>
					</table>
				</xsl:for-each-group>
			</body>
		</html>
	</xsl:template>
	<xsl:template name="Interpretes">
		<table width="100%">
			<xsl:for-each select=".">
				<xsl:apply-templates select="Interprete"/>
			</xsl:for-each>
		</table>
	</xsl:template>
	<xsl:template match="Interprete">
		<tr>
			<td>
				<xsl:value-of select="/AsMinhasMusicas/Interpretes/Interprete[@id_i=current()/@id_i]/Nome_i"/>
			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>
