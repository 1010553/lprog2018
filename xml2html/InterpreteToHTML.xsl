<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:import-schema schema-location="../Music.xsd"/>
	<xsl:output method="html" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/AsMinhasMusicas">
		<html>
			<body>
				<h1>Listagem por interpretes</h1>
				<span>Listagem de interpretes por nome (alfabeticamente)</span>
				<table border="1" width="100%">
					<tr align="center">
						<th>Interprete</th>
						<xsl:apply-templates select="Interpretes/Interprete">
							<xsl:sort select="Nome_i" data-type="text" order="ascending"/>
						</xsl:apply-templates>
					</tr>
				</table>
				<table border="1" width="100%">
				</table>
				<br/>
				<br/>
				<h1>Listagem de Musicas (Análise)</h1>
				<span>
                        Apresenta uma analise aos variados dados existente na coleção
                        <p/>
				</span>
				<table border="1" width="200">
					<tr>
						<td width="50%" align="center">
							<table border="0" width="200">
								<tr>
									<td style="text-align:left">
										<b>Número total de albuns :</b>
									</td>
									<td style="text-align:left">
										<xsl:value-of select="count(Albums/Album)"/>
									</td>
								</tr>
								<tr>
									<td style="text-align:left">
										<b>Número total faixas de músicas :</b>
									</td>
									<td style="text-align:left">
										<xsl:value-of select="count(Albums/Album/Musica)"/>
									</td>
								</tr>
								<tr>
									<td style="text-align:left">
										<b>Número de Interpretes :</b>
									</td>
									<td style="text-align:left">
										<xsl:value-of select="count(Interpretes/Interprete)"/>
									</td>
								</tr>
								<tr>
									<td style="text-align:left">
										<b>Número de músicas do género pop :</b>
									</td>
									<td style="text-align:left">
										<xsl:value-of select="count(Albums/Album/Musica[Genero='Pop'])"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br></br>
				<span>
                        Apresenta as lojas de onde foi feito o download de musicas do género 'Rock' presentes na coleção
                        <p/>
				</span>
				<table border="1">
					<tr bgcolor="#9acd32">
						<th>Musica</th>
						<th>Formato</th>
					</tr>
					<xsl:for-each select="Albums/Album/Musica">
						<tr>
							<td>
							<xsl:value-of select="Ficheiro/Formato"/></td>
							
							<xsl:choose>
								<xsl:when test="Genero='Rock'">
									<td bgcolor="#ff00ff">
										<xsl:value-of select="Ficheiro/Download/Loja"/>
									</td>
								</xsl:when>
								<xsl:otherwise>
								
											<td>
											<xsl:value-of select="Ficheiro/Download/Loja"/>
												</td>
								</xsl:otherwise>
							</xsl:choose>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="Interpretes">
		<xsl:element name="Interpretes">
			<xsl:apply-templates select="Interprete"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Interprete">
		<xsl:element name="Interprete">
			<tr>
				<td with="5%" align="middle" style="font-size:10px">
					<xsl:apply-templates select="Nome_i"/>
				</td>
			</tr>
			<xsl:apply-templates select="id_i"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Nome_i">
		<xsl:element name="Nome_i">
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="id_i">
		<xsl:attribute name="id_i">
		</xsl:attribute>
	</xsl:template>
	
<xsl:template match="Albums">
		<xsl:element name="Albums">
			<xsl:apply-templates select="Album"/>
		</xsl:element>
	</xsl:template>
	
	
	<xsl:template match="Album">
		<xsl:element name="Album">
			<xsl:apply-templates select="Musica"/>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="Musica">
		<xsl:element name="Musica">
			<tr>
				<td with="5%" align="middle" style="font-size:10px">
					<xsl:apply-templates select="Nome"/>
				</td>
			</tr>
			<xsl:apply-templates select="Faixa"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Nome">
		<xsl:element name="Nome">
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Faixa">
		<xsl:attribute name="Faixa">
		</xsl:attribute>
	</xsl:template>
	
	
	
</xsl:stylesheet>
