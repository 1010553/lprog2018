<?xml version="1.0" encoding="UTF-8"?>
<!-- ZZZ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:nsr="http://www.dei.isep.ipp.pt/lprog" version="1.0">
	<xsl:output method="html"/>
	<xsl:template match="/">
		<html>
			<head>
				<xsl:call-template name="header"/>
			</head>
			<body>
				<xsl:call-template name="banner"/>
				<xsl:apply-templates/>
			</body>
		</html>
	</xsl:template>
	<xsl:template name="header">
	<div align="center">
		<link href="../css/estilos.css" rel="stylesheet" type="text/css"/>
		<title>Relatório - LPROG 2NA 2017/2018</title>
		</div>
	</xsl:template>
	<xsl:template name="banner">
		<div align="center">
		<a name="home"/>
		<div id="banner">
			<a href="http://www.isep.ipp.pt/" target="_blank">
				<img href="images/logo_ISEP.jpg"/>
			</a>
		</div>
		</div>
	</xsl:template>
	<xsl:template match="nsr:páginaRosto">
	<div align="center">
		<div id="caixilho">
			<h1>
				<xsl:value-of select="nsr:tema"/>
			</h1>
			<h2>
				<xsl:apply-templates select="nsr:disciplina"/>
			</h2>
			<a href="http://www.dei.isep.ipp.pt/" target="_blank">
				<img src="{nsr:logotipoDEI}" width="512px" height="219px"/>
			</a>
			<xsl:call-template name="autores"/>
			<p/>
			<xsl:call-template name="professores"/>
			<p/>
			<xsl:call-template name="dataTurmaProf"/>
			<p/>
		</div>
			</div>
	</xsl:template>
	<xsl:template match="nsr:disciplina">
		<p>
			<xsl:value-of select="nsr:designação"/>
		</p>
		<p>
			<xsl:value-of select="nsr:anoCurricular"/>º Ano
		</p>
		<p>
			<xsl:value-of select="nsr:sigla"/>
		</p>
	</xsl:template>
	<xsl:template name="autores">
		<table class="infoAutores">
			<tbody>
				<xsl:for-each select=".">
					<xsl:apply-templates select="nsr:autor"/>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
	<xsl:template match="nsr:autor">
		<tr>
			<td>
				Nome:<xsl:value-of select=" nsr:nome"/>
			</td>
			<td>
				Nº:<xsl:value-of select=" nsr:número"/>
			</td>
			<td>
				E-mail:<xsl:value-of select=" nsr:mail"/>
			</td>
		</tr>
	</xsl:template>
	<xsl:template name="professores">
		<table class="infoProfessores">
			<tbody>
				<tr align="center">
					<th>Docentes</th>
				</tr>
				<xsl:for-each select=".">
					<xsl:apply-templates select="nsr:professor"/>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
	<xsl:template match="nsr:professor">
		<tr>
			<td>
				Sigla:  <xsl:value-of select="@sigla"/>
			</td>
			<td>
				Tipo de Aula:  <xsl:value-of select="@tipoAula"/>
			</td>
		</tr>
	</xsl:template>
	<xsl:template name="dataTurmaProf">
		<table class="infoAutores">
			<tbody>
				<tr>
					<td>
						<xsl:value-of select="nsr:data"/>
					</td>
					<td>
						<xsl:value-of select="nsr:turma"/>
					</td>
				</tr>
			</tbody>
		</table>
		<hr/>
		<a name="indice"/>
		<h1> Índice </h1>
		<ul>
			<xsl:for-each select="//*[@id]">
				<xsl:variable name="vref" select="@id"/>
				<xsl:if test="./@tituloSecção">
					<li>
						<a href="#{$vref}">
							<xsl:value-of select="./@tituloSecção"/>
						</a>
					</li>
				</xsl:if>
			</xsl:for-each>
		</ul>
		<hr/>
	</xsl:template>

	<xsl:template match="nsr:introdução|nsr:análise|nsr:linguagem|nsr:transformações|nsr:conclusão">
		<h2>
		<div align="center">
			<xsl:value-of select="./@id"/>
			<xsl:variable name="vRef" select="./@id"/>
			<a name="{$vRef}"/>
			<xsl:text>: </xsl:text>
			<xsl:value-of select="./@tituloSecção"/>
</div>		
		</h2>
		<div align="center">
		<xsl:apply-templates/>
				<xsl:call-template name="linksFundo"/>
				</div>
		<hr/>
	</xsl:template>
	
	<xsl:template match="nsr:outrasSecções">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="nsr:parágrafo">
		<xsl:apply-templates/>
		<p/>
	</xsl:template>
	<xsl:template match="nsr:referências">
	<div align="center">
	<hr id="linha"/>
		<div id="caixilho">
		<h2>
			<xsl:value-of select="./@id"/>
			<xsl:variable name="vRef" select="./@id"/>
			<a name="{$vRef}"/>
			<xsl:text>: </xsl:text>
			<xsl:value-of select="./@tituloSecção"/>
		</h2>
		<xsl:apply-templates/>
		<hr/>
		</div>
		</div>
	</xsl:template>
	
	<xsl:template match="nsr:refWeb">
	<div align="center">
	<p>
		<br></br>
			<br></br>
	<h3>
			<xsl:value-of select="./@idRef"/>
			<xsl:variable name="vRef" select="./@idRef"/>
			<a name="{$vRef}"/>
			<xsl:text>: </xsl:text>
			<xsl:value-of select="./@tituloSecção"/>
			</h3>
			<br>
				<a href="{nsr:URL}" target="_blank">
					<xsl:value-of select="nsr:URL"/>
				</a>
			</br>
			<br>
				<xsl:value-of select="nsr:descrição"/>
			</br>
			<br>
				<xsl:value-of select="nsr:consultadoEm"/>
			</br>
			<br></br>
			<br></br>
		</p>
		</div>
	</xsl:template>
	
	<xsl:template name="linksFundo">
		<table class="links">
			<tbody>
				<tr>
					<td>
						<a href="#home">Home</a>
					</td>
					<td>
						<a href="#indice">Índice</a>
					</td>
				</tr>
			</tbody>
		</table>
		<br/>
	</xsl:template>
</xsl:stylesheet>
